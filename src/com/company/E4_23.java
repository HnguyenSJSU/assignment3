/**
 * This program requires user to enter 2 inputs to calculate the
 * volume and area of a given soda can.
 * @author Hung Nguyen
 * @id 013626210
 * @course Cs-49J Sec 01
 * @title Assignment 3
 */


package com.company;
import java.util.Scanner;
class SodaCan {
    //fields to store user input
    private double mHeight;
    private double mDiameter;

    /**
     * Constructor to take in user input
     * @param: dHeightTemp:double dDiameterTemp:double
     * @return: none
     */
    public SodaCan(double dHeightTemp, double dDiameterTemp ) {
        mHeight = dHeightTemp;
        mDiameter = dDiameterTemp;
        this.getVolume();
        this.getSurfaceArea();
    }

    /**
     * Calculate volume by first finding the radius before
     * using PIr^2h to find the volume.
     * @param: none
     * @return: none
     */
    private void getVolume() {
        //find radius^2 from diameter
        double dRadiusSqrd = Math.pow(this.mDiameter/2,2);
        //find volume using formula
        double dVolume = Math.PI * dRadiusSqrd * this.mHeight;
        System.out.println(String.format("Soda Can Volume: %.2f", dVolume));
    }
    /**
     * Calculate volume by first finding the radius before
     * using 2pirh + 2pir^2h to find the volume.
     * @param: none
     * @return: none
     */
    private void getSurfaceArea(){
        //find radius from diameter
        double dTempRadius = this.mDiameter/2;
        //find area using formula
        double dArea = 2*Math.PI*(dTempRadius)*mHeight + 2*Math.PI*(Math.pow(dTempRadius,2));
        System.out.println(String.format("Soda Can Area: %.2f", dArea));
    }
    /**
     * set mHeight
     * @param: dTemp:double
     * @return: none
     */
    public void setmHeight(double dTemp){
        this.mHeight = dTemp;
    }
    /**
     * return height
     * @param: none
     * @return: this.mHeight:double
     */
    public double getmHeight(){
        return this.mHeight;
    }
    /**
     * set mDiameter
     * @param: dTemp:double
     * @return: none
     */
    public void setmDiameter(double dTemp){
        this.mDiameter = dTemp;
    }
    /**
     * return diameter
     * @param: none
     * @return: this.mHeight:double
     */
    public double getmDiameter(){
        return this.mDiameter;
    }


}
/**
 * This method asks user to input a height and diameter of a soda can.
 * a SodaCan instance will be created for those inputs
 * */
public class E4_23 {

    public static void main(String[] args) {
        //create instance of Scanner class
        Scanner in = new Scanner(System.in);

        System.out.println("Enter Soda Can’s height and diameter, use space to separate elements: ");
        //input a string line
        String strInput = in.nextLine();
        //splitting string into array
        String[] arrInput = strInput.split(" ", 2);
        //parse array input into a double
        double dHeight = Double.parseDouble(arrInput[0]);
        double dDiameter = Double.parseDouble(arrInput[1]);
        //create instance of soda can
        SodaCan canOne = new SodaCan(dHeight,dDiameter);
        //close instance
        in.close();
    }
}

