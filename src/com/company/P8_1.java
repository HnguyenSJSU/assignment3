/**
 * This program checks if the user are able to open a lock given their code
 * @author Hung Nguyen
 * @id 013626210
 * @course Cs-49J Sec 01
 * @title Assignment 3
 */

package com.company;
import java.util.InputMismatchException;
import java.util.Scanner;
class ComboLock {
    //mSecrets store the lock combination
    private final int mSecret1, mSecret2, mSecret3 ;
    //store current dial
    private int mCurrentDial = 0;
    //boolean flag to check if combinations are correct
    private int mSuccess1,mSuccess2,mSuccess3 = 0;

    /**
     * Constructor to create lock sequence
     * @param: secret1: int, secret2: int, secret3: int
     * @return: none
     */
    public ComboLock(int secret1, int secret2, int secret3) {
        this.mSecret1 = secret1;
        this.mSecret2 = secret2;
        this.mSecret3 = secret3;
    }
    /**
     * reset the lock to original form
     * @param: none
     * @return: none
     */
    public void reset() {
        this.mCurrentDial = 0;
        this.mSuccess1 =0;
        this.mSuccess2=0;
        this.mSuccess3 = 0;
    }

    /**
     * Turn left the amount of ticks
     * @param ticks
     * @return none
     */
    public void turnLeft(int ticks) {
        //add current dial to ticks
        this.mCurrentDial += ticks;
        //if dial is bigger than 40, mod 40 to get the ticks from 0
        if (this.mCurrentDial >= 40) {
            this.mCurrentDial %=40;
        }
        //set flag success to true
        if(this.mCurrentDial == mSecret2) {
            this.mSuccess2 = 1;
        }
    }

    /**
     * Turn right the amount of ticks
     * @param ticks
     * @return none
     */
    public void turnRight(int ticks) {
        //find the amount of ticks needed to turn
        int nTemp = mCurrentDial - ticks;
        //if ticks is < 0, reset mCurrentDial to 40
        if (nTemp < 0) {
            this.mCurrentDial = 40;
        }
        //minus the amount of steps from temp
        this.mCurrentDial -= Math.abs(nTemp);

        //set flag to true
        if(this.mCurrentDial == mSecret1) {
            mSuccess1= 1;

        }
        //set flag3 to true if the 3rd turn is correct
        if(mSuccess1 == 1) {
            if (this.mCurrentDial == mSecret3) {
                mSuccess3 = 1;
            }
        }


    }

    /**
     * check if all 3 turns are correct
     * params none
     * @return true/false
     */
    public boolean open() {
        if((mSuccess2 == 1) && (mSuccess1==1) && (mSuccess3==1)) {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * set flag to 1 if true
     * @params nTemp:int
     * @return none
     */
    public void setmSuccess1(int nTemp) {
        this.mSuccess1 = nTemp;
    }
    /**
     * return flag mSuccess1
     * @params none
     * @return mSuccess1
     */
    public int getmSuccess1() {
        return this.mSuccess1 ;
    }
    /**
     * set flag to 1 if true
     * @params nTemp:int
     * @return none
     */
    public void setmSuccess2(int nTemp) {
        this.mSuccess2 = nTemp;
    }
    /**
     * return flag mSuccess2
     * @params none
     * @return mSuccess2
     */
    public int getmSuccess2() {
        return this.mSuccess2 ;
    }
    /**
     * set flag to 1 if true
     * @params nTemp:int
     * @return none
     */
    public void setmSuccess3(int nTemp) {
        this.mSuccess3 = nTemp;
    }
    /**
     * return flag mSuccess3
     * @params none
     * @return mSuccess3
     */
    public int getmSuccess3() {
        return this.mSuccess3 ;
    }
    /**
     * upcate CurrentDial
     * @params nTemp:int
     * @return none
     */
    public void setCurrentDial(int nTemp) {
        this.mCurrentDial = nTemp;
    }
    /**
     * get CurrentDial and return
     * @params none
     * @return mCurrentDial
     */
    public int getCurrentDial() {
        return this.mCurrentDial;
    }
}

/**
 * class P8_1 creates a lock1 object
 * asks users to enter in order of right left right
 * if the user fails to enter the correct amount of ticks, return comment
 */
public class P8_1 {
    public static void main(String[] args) {
        //create a lock object
        ComboLock lock1 = new ComboLock(10, 15, 30);
        //create scanner object
        Scanner in = new Scanner(System.in);
        //Store user input
        int nTurn= 0;
        //Loop condition
        boolean bRep = true;
        while(bRep) {
            try {
                //ask for user input
                System.out.print("Turn right: ");
                nTurn = in.nextInt();
                lock1.turnRight(nTurn); //30

                System.out.print("Turn left: ");
                nTurn = in.nextInt();
                lock1.turnLeft(nTurn); //45

                System.out.print("Turn right: ");
                nTurn = in.nextInt();
                lock1.turnRight(nTurn); //25
                bRep = false;
            }
            // if user input wrong data
            catch(InputMismatchException e){
                System.out.println("Restarting progress, please enter digits");
                in.nextLine();
                lock1.reset();
            }

        }

        //check if user has opened the lock successfully
        boolean bCheck = lock1.open();
        if (bCheck == true) {
            System.out.println("The lock is opened");
        }
        else {
            System.out.println("The lock still locked");
        }
        in.close();

    }
}
